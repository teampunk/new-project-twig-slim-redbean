<?php
use \RedBeanPHP\Facade as R;
require 'vendor/autoload.php';

$host = "localhost";
$dbname = "interarq"; //Cambiar por el nombre de su base de datos.
$port = "3306"; //Puerto por default.
$user_db = "root"; //Usuario de base de datos con permisos exclusivos.
$password = "root"; //Contraseña de usuario.
try {
    R::setup("mysql:host=".$host.";dbname=".$dbname.";port=".$port,$user_db,$password);
    R::freeze(true);
    R::debug(false);
} catch (Exception $e ) {
    echo $e->getMessage();
}